import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  ImageBackground,
  Image
} from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {Home, Login, About, Buku} from '../pages'

const logo_home = require('../assets/home_icon.png')
const logo_about_us = require('../assets/about_us.png')
const logo_sign_out = require('../assets/icon_sign_out.png')

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const BottomNav = ({route}) => (
  <Tab.Navigator
  screenOptions={({ route }) => ({
      tabBarIcon: ({ }) => {
        let iconName;

        if (route.name === 'Home') {
          iconName = logo_home;
        } else if (route.name === 'About') {
          iconName = logo_about_us;
        } else if (route.name === 'SignOut') {
          iconName = logo_sign_out;
        }else{
          iconName = logo_sign_out;
        }

        return <Image source={iconName} style={{width:40}} />;
      },
      })}
      tabBarOptions={{
      activeTintColor: 'tomato',
      inactiveTintColor: 'gray',
      }}

  >
    <Tab.Screen name="Home" component={Home}  />
    <Tab.Screen name="About" component={About}  options={{
      headerTitle: 'About',
    }}  />
    <Tab.Screen name="SignOut" component={About}  />
  </Tab.Navigator>
);

const Router = ({}) => (
  <Stack.Navigator initialRouteName="BottomNav">
    <Stack.Screen name="Login" component={Login} options={{headerShown: false}} />
    <Stack.Screen name="Home" component={BottomNav}  options={{
      headerTitleStyle: {
        alignSelf: 'center'
      },
      headerStyle: {
        backgroundColor: '#F2F0F0',
      }, }} />
  </Stack.Navigator>
);

export default Router;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  logo:{

  }
});
