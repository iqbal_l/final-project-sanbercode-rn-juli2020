import React, { useState, createContext } from "react";
import Cookies from 'js-cookie'

import {Redirect} from 'react-router-dom'

export const LoginContext = createContext();
export const LoginChangeContext = createContext();
export const LoginSubmitContext = createContext();
export const LoginWarningContext = createContext();
export const LoginGagalContext = createContext();
export const LoginLogoutContext = createContext();

export const LoginProvider = props => {
  const [isLogin, setLogin] = useState(Cookies.get('access_token'));
  const [input, setInput] = useState({user:"",password:""})
  const [isGagal, setGagal] = useState({user:"",password:""})
  const [warning, setWarning] = useState('');

  const handleChange = (event) => {
    let typeOfInput = event.target.name


    switch (typeOfInput) {
      case 'user': setInput({...input,user:event.target.value}); break;
      case 'password': setInput({...input, password:event.target.value}); break;
      default:break;
    }
  }

  const handleLogout = (event) => {
      Cookies.set('access_token','0')
      setLogin('0')
  }


  const handLeLogin = (event) =>{
    event.preventDefault()

    setWarning("")
    setGagal([])
    setLogin('0')

    let username = input.user;
    let password = input.password;
    let cekuser = '';
    let cekpass = '';

    if (username !== '' &&  password !== ''){
      if (username.toLowerCase() !== 'admin' && password !== 'admin'){
        setWarning("Username atau Password Anda salah")
      }else{
        setLogin('1')
        Cookies.set('access_token','1')
      }
    }else{
      if (username === ''){
        cekuser = 'Username belum diisi'
      }
      if (password === ''){
        cekpass = 'Password belum diisi'
      }

      setGagal({user:cekuser,password:cekpass})
    }
  }



  return (

    <>
      <LoginContext.Provider value={[isLogin, setLogin]}>
        <LoginChangeContext.Provider value={[handleChange]}>
          <LoginSubmitContext.Provider value={[handLeLogin]}>
            <LoginWarningContext.Provider value={[warning, setWarning]}>
              <LoginGagalContext.Provider value={[isGagal, setGagal]}>
                <LoginLogoutContext.Provider value={[handleLogout]}>
                  {props.children}
                </LoginLogoutContext.Provider>
              </LoginGagalContext.Provider>
            </LoginWarningContext.Provider>
          </LoginSubmitContext.Provider>
        </LoginChangeContext.Provider>
      </LoginContext.Provider>
    </>
  );
};
