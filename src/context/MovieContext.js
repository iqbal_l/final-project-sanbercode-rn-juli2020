import React, { useState, createContext, useEffect } from "react";
import axios from 'axios';

export const MovieContext = createContext([]);
export const MovieHapusContext = createContext();
export const MovieEditContext = createContext();
export const MovieSelectedContext = createContext();
export const MovieStatusFormContext = createContext();
export const MovieInputContext = createContext();
export const MovieChangeContext = createContext();

export const MovieProvider = props => {
  const [movie, setMovie] = useState([]);
  const [input, setInput] = useState({rating:'0',genre:'',duration:'0',description:"",title:"",year:""})
  const [selectedId, setSelectedId]  =  useState(0)
  const [statusForm, setStatusForm]  =  useState("create")

  useEffect( () => {
    if (movie.length === 0){
       axios.get(`http://backendexample.sanbercloud.com/api/movies`)
       .then(res => {
         console.log(res)
         setMovie(res.data.map(el => {return {
            id:el.id,
            title:el.title,
            rating:el.rating,
            genre:el.genre,
            duration:el.duration,
            description:el.description,
            year:el.year
          }}))
       })
     }
   })

   const handleChange = (event) => {
     let typeOfInput = event.target.name

     switch (typeOfInput) {
       case 'title':
         setInput({...input,title:event.target.value})
         break;
       case 'description':
         setInput({...input,description:event.target.value})
         break;
       case 'duration':
         setInput({...input,duration:event.target.value})
         break;
       case 'genre':
         setInput({...input,genre:event.target.value})
         break;
       case 'rating':
         setInput({...input,rating:event.target.value})
         break;
       case 'year':
         setInput({...input,year:event.target.value})
         break;
       default:break;

     }
   }

   const handleHapus = (event) => {
     let idMovie = parseInt(event.target.value)
     let newDaftarMovie = movie.filter(el => el.id != idMovie)

     axios.delete(`http://backendexample.sanbercloud.com/api/movies/${idMovie}`)
     .then(res => {
       console.log(res)
     })

     setMovie([...newDaftarMovie])

   }
   //
   const handleEdit  = (event) => {
     let idMovie = parseInt(event.target.value)
     let loadMovie = movie.find(x=> x.id === idMovie)
     setInput({
       title:loadMovie.title,
       rating:loadMovie.rating,
       description:loadMovie.description,
       genre:loadMovie.genre,
       duration:loadMovie.duration,
       year:loadMovie.year
     })
     setSelectedId(idMovie)
     setStatusForm("edit")

   }

  return (
    <>
    <MovieContext.Provider value={[movie, setMovie]}>
      <MovieHapusContext.Provider value={[handleHapus]}>
        <MovieEditContext.Provider value={[handleEdit]}>
          <MovieSelectedContext.Provider value={[selectedId, setSelectedId]}>
            <MovieStatusFormContext.Provider value={[statusForm, setStatusForm]}>
              <MovieInputContext.Provider value={[input, setInput]}>
                <MovieChangeContext.Provider value={[handleChange]}>
                  {props.children}
                </MovieChangeContext.Provider>
              </MovieInputContext.Provider>
            </MovieStatusFormContext.Provider>
          </MovieSelectedContext.Provider>
        </MovieEditContext.Provider>
      </MovieHapusContext.Provider>
    </MovieContext.Provider>
    </>
  );
};
