
import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  Dimensions,
  Image
} from 'react-native';


const DEVICE = Dimensions.get('window')
const reactlogo = require('../../assets/react_img.png')

const Home = ({}) => (
  <View style={styles.container}>
    <View style={{flexDirection:'column',flex:0.1}}>
      <View style={{flex:2,flexDirection:"row",justifyContent:'space-between', marginBottom: 10}}>
        <View style={{flex:0.3, marginRight: 5, marginTop:10}}>
          <Text>Cari Buku :
          </Text>
        </View>
        <View style={{flex:1, marginLeft: 5}}>
          <TextInput placeholder="ketikkan judul buku" style={styles.textInput} />
        </View>
      </View>
    </View>

    <View style={styles.itemContainer}>
      <View style={{backgroundColor:'#F2F0F0',flex:2,flexDirection:"row",justifyContent:'space-between', marginBottom: 10}}>
        <View style={{flex:0.5, padding:3,paddingTop:1}}>
          <Image source={reactlogo} />
        </View>
        <View style={{flex:0.7, padding:10,paddingTop:10}}>
          <Text>Harry Potter - 300 halaman</Text>
          <Text>Pengarang : Aku Sendiri</Text>
          <Text>Terbitan : Waktu</Text>
        </View>
      </View>

      <View style={{backgroundColor:'#F2F0F0',flex:2,flexDirection:"row",justifyContent:'space-between', marginBottom: 10}}>
        <View style={{flex:0.5, marginRight: 5, padding:3,paddingTop:1}}>
          <Image source={reactlogo} />
        </View>
        <View style={{flex:0.7, padding:10,paddingTop:10}}>
          <Text>Harry Potter - 300 halaman</Text>
          <Text>Pengarang : Aku Sendiri</Text>
          <Text>Terbitan : Waktu</Text>
        </View>
      </View>
    </View>




  </View>
);

export default Home;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding:10,
    backgroundColor: 'white'
  },
  textInput: {
    width: DEVICE.width * 0.7,
    height: 40,
    padding:10,
    backgroundColor: '#F2F0F0'
  },
  itemContainer:{
    flex:0.4,
    marginTop:10,
    width: DEVICE.width * 0.94,
    height: 105,
  }
});
