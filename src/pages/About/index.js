
import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  ImageBackground
} from 'react-native';

const version = require('../../assets/version.png')
const gitlab = require('../../assets/gitlab.png')

const About = (props) => (
  <View style={styles.container}>
    <View style={styles.containercenter}>
      <ImageBackground source={version} style={styles.logo}>
      </ImageBackground>
      <Text style={styles.penjelasan}>
      Aplikasi ini digunakan, untuk menginformasikan List buku, yang tersedia untuk dipinjam,
dan menginformasikan, sisa buku yang bisa
dipinjam
      </Text>
    </View>
    <View style={styles.containerbottom}>
    </View>
    <View style={styles.containerbottom}>
    </View>
    <View style={styles.containerbottom}>
    <View style={{flexDirection:'column',flex:1}}>
      <View style={{flex:2,flexDirection:"row",justifyContent:'space-between', marginBottom: 10}}>
        <View style={{flex:0.3, marginRight: 5, marginTop:10}}>

        </View>
        <View style={{flex:1, marginLeft: 5,}}>
          <Text style={{ fontSize:20, fontWeight:'bold'}}>
            Creator: M Iqbal laksana
          </Text>
          <View style={{flex:2,flexDirection:"row",justifyContent:'space-between', marginBottom: 10}}>
            <View style={{flex:0.1, marginRight: 5, marginTop:10}}>
              <ImageBackground source={gitlab} style={styles.logoicon} />
            </View>
            <View style={{flex:1, marginRight: 5, marginTop:10}}>
              <Text style={{ fontSize:15, fontWeight:'bold'}}>
                iqbal_l
              </Text>
            </View>
            </View>
        </View>
      </View>
    </View>
    </View>
  </View>
);

export default About;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'white'
  },
  penjelasan: {
    marginTop: 30,
    marginLeft: 10,
    fontWeight:'bold',
    textAlign :'justify',
    fontSize:20
  },
  containercenter: {
    marginTop: 20,
    flex: 1,
    padding:10
  },
  containerbottom: {
    flex: 0.5,
    padding:20,
    flexDirection:'row'
  },
  logo:{
    width: 319,
    height: 180,
  },
  logoicon:{
    width: 20,
    height: 20,
  },
});
