
import React from 'react';
import {
  View,
  Text,
  StyleSheet,
} from 'react-native';

const Buku = ({}) => (
  <View style={styles.container}>
    <Text>Buku</Text>
  </View>
);

export default Buku;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
