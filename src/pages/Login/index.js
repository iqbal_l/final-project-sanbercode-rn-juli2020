import React from 'react';
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  ImageBackground,
  Button
} from 'react-native';

const logo = require('../../assets/icon_library.png')

export default class Login extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      userName: '',
      password: '',
      isError: false,
    }
  }

  loginHandler() {
    const { navigation } = this.props;

    if (this.state.password !== 'admin' || this.state.userName !== 'admin'){
      this.setState({isError:true})
    }else{
      this.setState({isError:false})
      navigation.navigate('Home');
    }
  }

  render() {
    return (
    <View style={styles.container}>
      <View style={styles.containercenter}>
        <ImageBackground source={logo} style={styles.logo}>
        </ImageBackground>
        <TextInput
            style={styles.textInput}
            placeholder='EMAIL'
            onChangeText={userName => this.setState({ userName })}
        />
        <TextInput
            style={styles.textInput}
            placeholder='PASSWORD'
            onChangeText={password => this.setState({ password })}
            secureTextEntry={true}
        />
        <Text style={this.state.isError ? styles.errorText : styles.hiddenErrorText}>Password Salah</Text>
        <View style={styles.btnLogin}>
          <Button title="LOGIN" color="#555555" onPress={() => this.loginHandler()}  />
        </View>
      </View>
      <View style={styles.containerbottom}>
      </View>
    </View>
  );
}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'white'
  },
  containercenter: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  containerbottom: {
    flex: 0.5,
  },
  logo:{
    width: 319,
    height: 126,
  },
  textInput: {
    marginTop: 20,
    width: 319,
    height: 40,
    padding:10,
    backgroundColor: '#F2F0F0'
  },
  btnLogin:{
    marginTop:10,
    width: 319,
    height: 40,
    backgroundColor: '#555555',
    shadowColor: "#555555",
    shadowOffset: {
    	width: 10,
    	height: 1,
    },
    shadowOpacity: 0.5,
    elevation: 5,
  },
  errorText: {
    color: 'red',
    textAlign: 'center',
    marginBottom: 16,
    display: 'flex'
  },
  hiddenErrorText: {
    color: 'transparent',
    textAlign: 'center',
    marginBottom: 16,
    display: 'none'
  }
});
