# Tentang Aplikasi
link commit : https://gitlab.com/iqbal_l/final-project-sanbercode-rn-juli2020/-/commits/master

1. Aplikasi ini bertujuan untuk, melihat list buku dair sebuah perpustakaan, agar kita mengetahui, buku mana saja yang sudah di booking atau dipinjam
2. Terdiri dari 4 halaman, login, beranda, detail dan about us
3. API = tidak digunakan pada aplikasi ini, masih menggunkan dummy data

# Spesifikasi
1. Dibangun menggunakan EXPO CLI
2. Emulator yang digunakan menggunakan handphone Redmi Note 4
3. Editor Atom
4. untuk command line, menggunakan cmder


# Mockupo Figma
link : https://www.figma.com/file/efCn1TOuCVhu5U3ITauCUG/Untitled?node-id=0%3A1
